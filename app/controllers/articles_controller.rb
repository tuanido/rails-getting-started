class ArticlesController < ApplicationController

  http_basic_authenticate_with name:"sergio", password: "changeit", except: [:index, :show]

  def index
    @articles = Article.all
  end

  def show
    current_article
  end

  def new
    @article = Article.new
  end

  def edit
    current_article
  end

  def create
    if new_article.save
      redirect_to (new_article)
    else
      render :new
    end
  end

  def update
    if current_article.update (article_params)
      redirect_to (current_article)
    else
      render :edit
    end
  end

  def destroy
    current_article.destroy
    redirect_to articles_path
  end

  private
  def article_params
    params.require(:article).permit(:title, :text)
  end

  def current_article
    @article = Article.find (params[:id])
  end

  def new_article
    @article ||= Article.new (article_params)
  end

end
